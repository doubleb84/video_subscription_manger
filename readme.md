# Wymagania
- PHP 7.2
- Composer
- Redis (4.0.9)

# Instalacja

Aby zainstalować projekt pobierz go z repozytorium.

Nastepnie wywołaj komendę w katalogu gdzie skonowałeś projekt
`composer install`

W pliku `.env` katalogu glownym projektu znajduje sie konfiguracja redisa
pod zmienna srodowiskowa `REDIS_URL`

# Endpoint

- /user
  
  - Metoda `POST` tworzy nowego usera - akceptowany jest json strukturze `{"username":"test@email.pl"}`. Pole `usename` jest emailem
  
  - Metoda `DELETE` kasuje wskazanego uzytkownika - akceptowany jest json strukturze `{"username":"test@email.pl"}`
    

- /resource
    
    - Metoda `POST` tworzy nowy zasob - akceptowany jest json o strukturze `{"resource_name":"test2"}`
      
      W odpowiedzi dostajemy infomacje z indetyfikatorem zasobu - uuid lub komunikat o błędzie
      
 - /resource/relationship
    
    - Metoda `POST` tworzy nowa relacje miedzy zasobami - akceptowany jest json o strukturze `{"parent_id":"uuid","child_id":"uuid"}`
     
     W odpowiedzi dosjemy informacje o sukcesie lub błędzie
     
 - /entitlement
    
    -  Metoda `POST` tworzy nowe uprawnienie do danego zasobu dla uzytkownia. 
       
       Akceptowany jest json o strukturze `{"username":"example@email","resource_id": "uuid"}`
   
    - Metoda `DELETE` usuwa uprawnienie do danego zasobu dla uzytkownika
    
      Akceptowany jest json o strukturze `{"username":"example@email","resource_id": "uuid"}`
      
 - /entitlement/check
    
    - Metoda `GET` sprawdza uprawniena do danego zasobu dla uzytkownika
     
      Akceptowany jest json o strukturze `{"username":"example@email","resource_id": "uuid"}`
      
     


