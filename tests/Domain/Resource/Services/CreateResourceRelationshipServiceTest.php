<?php

namespace App\Tests\Domain\Resource\Services;

use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Exceptions\ResourceNotExistException;
use App\Domain\Resource\Exceptions\SameChildIdAndParentIdException;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\Resource\Services\CreateResourceRelationshipService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateResourceRelationshipServiceTest extends TestCase
{
    const UUID = '1121e2fc-e83d-4a20-9aa7-38eaa6ef512e';
    const UUID_2 = '92150665-b8a9-42d2-a90e-576bdc525dac';

    /** @var MockObject | ResourceRepositoryInterface */
    private $redisResourceRepositoryMock;

    /** @var CreateResourceRelationshipService */
    private $createResourceRelationshipService;

    public function setUp()
    {
        $this->redisResourceRepositoryMock = $this
            ->getMockBuilder(ResourceRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->createResourceRelationshipService = new CreateResourceRelationshipService($this->redisResourceRepositoryMock);
    }

    public function testExecuteShouldThrowExceptionWhenResourceAreSame()
    {
        $parentResource = new ResourceId(self::UUID);
        $childResource = new ResourceId(self::UUID);

        $this->expectException(SameChildIdAndParentIdException::class);
        $this->createResourceRelationshipService->execute($childResource, $parentResource);
    }

    public function testExecuteShouldThrowExceptionResourceNotExist()
    {
        $parentResource = new ResourceId(self::UUID);
        $childResource = new ResourceId(self::UUID_2);

        $this->expectException(ResourceNotExistException::class);

        $this->redisResourceRepositoryMock
            ->expects($this->any())
            ->method('isResourceExist')
            ->will($this->returnValueMap([[self::UUID, true], [self::UUID_2, false]]));

        $this->createResourceRelationshipService->execute($childResource, $parentResource);
    }

    public function testExecuteShouldPass()
    {
        $parentResource = new ResourceId(self::UUID);
        $childResource = new ResourceId(self::UUID_2);

        $this->redisResourceRepositoryMock
            ->expects($this->any())
            ->method('isResourceExist')
            ->will($this->returnValueMap([[self::UUID, true], [self::UUID_2, true]]));

        $this->redisResourceRepositoryMock->expects($this->once())->method('getResourceParentId')
            ->with(self::UUID_2);

        $this->redisResourceRepositoryMock->expects($this->once())->method('createRelationshipBetweenResources')
            ->with(self::UUID_2, self::UUID);

        $this->createResourceRelationshipService->execute($childResource, $parentResource);
    }
}
