<?php

namespace App\Tests\Domain\Resource\Services;

use App\Domain\Resource\Dto\Resource;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Dto\ResourceName;
use App\Domain\Resource\Exceptions\CreateResourceException;
use App\Domain\Resource\Exceptions\EmptyResourceNameException;
use App\Domain\Resource\Exceptions\InvalidUuidException;
use App\Domain\Resource\Exceptions\ResourceAlreadyExistException;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\Resource\Services\CreateResourceService;
use PHPUnit\Framework\TestCase;

class CreateResourceServiceTest extends TestCase
{
    private $resourceRepository;

    private $createResourceService;

    public function setUp()
    {
        $this->resourceRepository = $this->getMockBuilder(ResourceRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->createResourceService = new CreateResourceService($this->resourceRepository);
    }

    public function testExecuteShouldThrowExceptionForInvalidUUID()
    {
        $invalidUUID = '12345';

        $resourceUUID = new ResourceId($invalidUUID);
        $resourceName = new ResourceName('TEST');

        $resource = new Resource($resourceUUID, $resourceName);

        $this->expectException(InvalidUuidException::class);

        $this->createResourceService->execute($resource);
    }

    public function testExecuteShouldThrowExceptionForEmptyName()
    {
        $validUUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

        $resourceUUID = new ResourceId($validUUID);
        $resourceName = new ResourceName('');

        $resource = new Resource($resourceUUID, $resourceName);

        $this->expectException(EmptyResourceNameException::class);

        $this->createResourceService->execute($resource);
    }

    public function testExecuteShouldThrowExceptionWhenResourceExist()
    {
        $validUUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

        $resourceUUID = new ResourceId($validUUID);
        $resourceName = new ResourceName('TEST');

        $resource = new Resource($resourceUUID, $resourceName);

        $this->resourceRepository->method('isResourceExist')
            ->with($validUUID)
            ->willReturn(true);

        $this->expectException(ResourceAlreadyExistException::class);

        $this->createResourceService->execute($resource);
    }

    public function testExecuteShouldThrowExceptionWhenResourceFailCreation()
    {
        $validUUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

        $resourceUUID = new ResourceId($validUUID);
        $resourceName = new ResourceName('TEST');

        $resource = new Resource($resourceUUID, $resourceName);

        $this->resourceRepository->method('isResourceExist')
            ->with($validUUID)
            ->willReturn(false);

        $this->resourceRepository->method('save')
            ->with($validUUID, 'TEST')
            ->willReturn(false);

        $this->expectException(CreateResourceException::class);

        $this->createResourceService->execute($resource);
    }

    public function testExecuteShouldCreateResource()
    {
        $validUUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

        $resourceUUID = new ResourceId($validUUID);
        $resourceName = new ResourceName('TEST');

        $resource = new Resource($resourceUUID, $resourceName);

        $this->resourceRepository->method('isResourceExist')
            ->with($validUUID)
            ->willReturn(false);

        $this->resourceRepository->method('save')
            ->with($validUUID, 'TEST')
            ->willReturn(true);

        $resourceReturned = $this->createResourceService->execute($resource);
        $this->assertSame($resource, $resourceReturned);
    }
}
