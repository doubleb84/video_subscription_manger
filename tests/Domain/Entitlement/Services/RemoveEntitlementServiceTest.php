<?php

namespace App\Tests\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Entitlement\Exceptions\UserNotExistException;
use App\Domain\Entitlement\Repository\EntitlementRepositoryInterface;
use App\Domain\Entitlement\Services\RemoveEntitlementService;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Dto\Email;
use App\Domain\User\Repository\UserRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RemoveEntitlementServiceTest extends TestCase
{
    const RESOURCE_UUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

    /** @var MockObject | UserRepositoryInterface */
    private $userRepositoryMock;

    /** @var MockObject | ResourceRepositoryInterface */
    private $resourceRepositoryMock;

    /** @var MockObject | EntitlementRepositoryInterface */
    private $entitlementRepositoryMock;

    /** @var RemoveEntitlementService */
    private $removeEntitlementService;

    public function setUp()
    {
        $this->userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resourceRepositoryMock = $this->getMockBuilder(ResourceRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entitlementRepositoryMock = $this->getMockBuilder(EntitlementRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->removeEntitlementService = new RemoveEntitlementService($this->entitlementRepositoryMock, $this->userRepositoryMock, $this->resourceRepositoryMock);
    }

    public function testExecuteShouldFailWhenResourceNotExist()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);
        $email = new Email('test@wp.pl');

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(false);
        $this->expectException(ResourceNotExistException::class);

        $this->removeEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldFailWhenUserNotExist()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);
        $email = new Email('test@test.pl');

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with('test@test.pl')
            ->willReturn(false);

        $this->expectException(UserNotExistException::class);

        $this->removeEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldPass()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);
        $email = new Email('test@test.pl');

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with('test@test.pl')
            ->willReturn(true);

        $this->entitlementRepositoryMock->expects($this->once())
            ->method('remove')
            ->with(self::RESOURCE_UUID, 'test@test.pl');

        $this->removeEntitlementService->execute($entitlement);
    }
}
