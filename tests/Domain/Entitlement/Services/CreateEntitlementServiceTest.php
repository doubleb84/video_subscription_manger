<?php

namespace App\Tests\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Entitlement\Exceptions\UserNotExistException;
use App\Domain\Entitlement\Exceptions\WrongExpireDateException;
use App\Domain\Entitlement\Services\CreateEntitlementService;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Dto\Email;
use App\Domain\User\Repository\UserRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateEntitlementServiceTest extends TestCase
{
    const TEST_EMAIL = 'test@test.pl';
    const RESOURCE_UUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';

    /** @var MockObject | UserRepositoryInterface */
    private $redisUserRepositoryMock;

    /** @var MockObject | ResourceRepositoryInterface */
    private $resourceRepositoryMock;

    private $createEntitlementService;

    public function setUp()
    {
        $this->resourceRepositoryMock = $this->getMockBuilder(ResourceRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->redisUserRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->createEntitlementService = new CreateEntitlementService($this->redisUserRepositoryMock, $this->resourceRepositoryMock);
    }

    public function testExecuteShouldThrowExceptionWhenResourceNotExist()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);

        $email = new Email(self::TEST_EMAIL);

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(false);

        $this->expectException(ResourceNotExistException::class);

        $this->createEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldThrowExceptionWhenUserNotExist()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);

        $email = new Email(self::TEST_EMAIL);

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with(self::TEST_EMAIL)
            ->willReturn(false);

        $this->expectException(UserNotExistException::class);

        $this->createEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldThrowExceptionWhenDateIsInvalid()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);

        $email = new Email(self::TEST_EMAIL);

        $dateTime = new \DateTime('yesterday');

        $entitlement = new Entitlement($resourceId, $email, $dateTime);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with(self::TEST_EMAIL)
            ->willReturn(true);

        $this->expectException(WrongExpireDateException::class);

        $this->createEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldAddEntitlementForResource()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);

        $email = new Email(self::TEST_EMAIL);

        $entitlement = new Entitlement($resourceId, $email);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with(self::TEST_EMAIL)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('addResourceToUser')
            ->with(self::TEST_EMAIL, $resourceId);

        $this->createEntitlementService->execute($entitlement);
    }

    public function testExecuteShouldAddEntitlementWithExpireDateForResource()
    {
        $resourceId = new ResourceId(self::RESOURCE_UUID);

        $email = new Email(self::TEST_EMAIL);
        $expireDateTime = new \DateTime('now + 4 days');

        $entitlement = new Entitlement($resourceId, $email, $expireDateTime);

        $this->resourceRepositoryMock->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('isUserExist')
            ->with(self::TEST_EMAIL)
            ->willReturn(true);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('addExpireTimeToUserResource')
            ->with(self::TEST_EMAIL, $resourceId, $expireDateTime);

        $this->redisUserRepositoryMock->expects($this->once())
            ->method('addResourceToUser')
            ->with(self::TEST_EMAIL, $resourceId);

        $this->createEntitlementService->execute($entitlement);
    }
}
