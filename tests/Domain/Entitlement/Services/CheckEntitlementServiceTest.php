<?php

namespace App\Tests\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Entitlement\Services\CheckEntitlementService;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Dto\Email;
use App\Domain\User\Repository\UserRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CheckEntitlementServiceTest extends TestCase
{
    const RESOURCE_UUID = '1cf3b7af-dfdd-4ec5-a58f-7abdde17a61c';
    const PARENT_RESOURCE_UUID = '8a66c64b-2f9f-4311-b3eb-fa97f619d6d8';
    const OTHER_UUID = 'd2d606f3-6998-4ef6-81bc-03b7788a6c76';
    const EMAIL = 'test@test.pl';

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var ResourceRepositoryInterface
     */
    private $resourceRepository;

    private $checkEntitlementService;

    public function setUp()
    {
        $this->userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resourceRepository = $this->getMockBuilder(ResourceRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->checkEntitlementService = new CheckEntitlementService($this->userRepository, $this->resourceRepository);
    }

    public function testExecuteShouldThrowExceptionWhenResourceNotExist()
    {
        $resource = new ResourceId(self::RESOURCE_UUID);
        $email = new Email(self::EMAIL);

        $entitlement = new Entitlement($resource, $email);

        $this->expectException(ResourceNotExistException::class);

        $this->checkEntitlementService->execute($entitlement);
    }

    public function testExecuteIfResourceIsInAllowedListShouldReturnTrue()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);

        $entitlement = new Entitlement($resource, $email);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::RESOURCE_UUID, self::OTHER_UUID]);

        $this->userRepository->expects($this->once())
            ->method('getExpireDateOfUserResource')
            ->with(self::EMAIL, self::RESOURCE_UUID)
            ->willReturn([self::RESOURCE_UUID => false]);

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertTrue($result);
    }

    public function testExecuteIfResourceIsInAllowedListAndExpireDateIsValidShouldReturnTrue()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);

        $dateTime = new \DateTime('now + 1 day');
        $entitlement = new Entitlement($resource, $email, $dateTime);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::RESOURCE_UUID, self::OTHER_UUID]);

        $this->userRepository->expects($this->once())
            ->method('getExpireDateOfUserResource')
            ->with(self::EMAIL, self::RESOURCE_UUID)
            ->willReturn([self::RESOURCE_UUID => $dateTime->format('Y-m-d H:i:s')]);

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertTrue($result);
    }

    public function testExecuteIfResourceIsInAllowedListAndExpireDateIsNotValidShouldReturnFalse()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);

        $dateTime = new \DateTime('now - 1 day');
        $entitlement = new Entitlement($resource, $email, $dateTime);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::RESOURCE_UUID, self::OTHER_UUID]);

        $this->userRepository->expects($this->once())
            ->method('getExpireDateOfUserResource')
            ->with(self::EMAIL, self::RESOURCE_UUID)
            ->willReturn([self::RESOURCE_UUID => $dateTime->format('Y-m-d H:i:s')]);

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertFalse($result);
    }

    public function testExecuteIfParentResourceIsAllowedShouldReturnTrue()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);
        $parentResource = new ResourceId(self::PARENT_RESOURCE_UUID);

        $entitlement = new Entitlement($resource, $email);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::PARENT_RESOURCE_UUID, self::OTHER_UUID]);

        $this->userRepository
            ->method('getExpireDateOfUserResource')
            ->will($this->returnValueMap(
                [
                [self::EMAIL, self::RESOURCE_UUID, [self::RESOURCE_UUID => false]],
                [self::EMAIL, self::PARENT_RESOURCE_UUID, [self::PARENT_RESOURCE_UUID => false]], ]
            ));

        $this->resourceRepository->method('getResourceParentId')
            ->will($this->returnValueMap([
                [self::RESOURCE_UUID, self::PARENT_RESOURCE_UUID],
                [self::PARENT_RESOURCE_UUID, null],
            ]));

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertTrue($result);
    }

    public function testExecuteIfParentResourceIsAllowedAndDateTimeIsValidShouldReturnTrue()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);
        $parentResource = new ResourceId(self::PARENT_RESOURCE_UUID);

        $entitlement = new Entitlement($resource, $email);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::PARENT_RESOURCE_UUID, self::OTHER_UUID]);

        $dateTimeExpire = new \DateTime('now + 1 day');
        $this->userRepository
            ->method('getExpireDateOfUserResource')
            ->will($this->returnValueMap(
                [
                    [self::EMAIL, self::RESOURCE_UUID, [self::RESOURCE_UUID => false]],
                    [self::EMAIL, self::PARENT_RESOURCE_UUID, [self::PARENT_RESOURCE_UUID => $dateTimeExpire->format('Y-m-d H:i:s')]], ]
            ));

        $this->resourceRepository->method('getResourceParentId')
            ->will($this->returnValueMap([
                [self::RESOURCE_UUID, self::PARENT_RESOURCE_UUID],
                [self::PARENT_RESOURCE_UUID, null],
            ]));

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertTrue($result);
    }

    public function testExecuteIfParentResourceIsNotAllowedShouldReturnFalse()
    {
        $email = new Email(self::EMAIL);

        $resource = new ResourceId(self::RESOURCE_UUID);

        $entitlement = new Entitlement($resource, $email);

        $this->resourceRepository->expects($this->once())
            ->method('isResourceExist')
            ->with(self::RESOURCE_UUID)
            ->willReturn(true);

        $this->userRepository->expects($this->once())
            ->method('getUserResources')
            ->with(self::EMAIL)
            ->willReturn([self::OTHER_UUID]);

        $dateTimeExpire = new \DateTime('now - 1 day');
        $this->userRepository
            ->method('getExpireDateOfUserResource')
            ->will($this->returnValueMap([
                [self::EMAIL, self::RESOURCE_UUID, [self::RESOURCE_UUID => false]],
                [self::EMAIL, self::PARENT_RESOURCE_UUID, [self::PARENT_RESOURCE_UUID => $dateTimeExpire->format('Y-m-d H:i:s')]],
            ]));

        $this->resourceRepository->method('getResourceParentId')
            ->will($this->returnValueMap([
                [self::RESOURCE_UUID, self::PARENT_RESOURCE_UUID],
                [self::PARENT_RESOURCE_UUID, null],
            ]));

        $result = $this->checkEntitlementService->execute($entitlement);
        $this->assertFalse($result);
    }
}
