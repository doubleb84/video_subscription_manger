<?php

namespace App\Tests\Domain\User;

use App\Domain\User\Dto\Email;
use App\Domain\User\Exceptions\UserAlreadyExistException;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\Services\CreateUserService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateUserServiceTest extends TestCase
{
    const EMAIL_ADDRESS = 'test@wp.pl';

    /** @var MockObject | UserRepositoryInterface */
    private $redisUserRepositoryMock;

    /** @var CreateUserService */
    private $createUserService;

    public function setUp()
    {
        $this->redisUserRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->createUserService = new CreateUserService($this->redisUserRepositoryMock);
    }

    public function testExecuteShouldThrowException()
    {
        $email = new Email(self::EMAIL_ADDRESS);

        $this->redisUserRepositoryMock
            ->expects($this->once())
            ->method('isUserExist')
            ->with(self::EMAIL_ADDRESS)
            ->willReturn(true);

        $this->expectException(UserAlreadyExistException::class);
        $this->createUserService->execute($email);
    }

    public function testExecuteShouldCreateUser()
    {
        $email = new Email(self::EMAIL_ADDRESS);

        $this->redisUserRepositoryMock
            ->expects($this->once())
            ->method('isUserExist')
            ->with(self::EMAIL_ADDRESS)
            ->willReturn(false);

        $this->redisUserRepositoryMock
            ->expects($this->once())
            ->method('createUser')
            ->with(self::EMAIL_ADDRESS);

        $this->createUserService->execute($email);
    }
}
