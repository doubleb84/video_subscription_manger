<?php

namespace App\Tests\Domain\User\Services;

use App\Domain\User\Dto\Email;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\Services\RemoveUserService;
use PHPUnit\Framework\TestCase;

class RemoveUserServiceTest extends TestCase
{
    const USER_EMAIL = 'test@test.pl';

    /** @var UserRepositoryInterface */
    private $userRepositoryInterface;

    /** @var RemoveUserService */
    private $removeUserService;

    public function setUp()
    {
        $this->userRepositoryInterface = $this->getMockBuilder(UserRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->removeUserService = new RemoveUserService($this->userRepositoryInterface);
    }

    public function testExecuteRemoveUser()
    {
        $email = new Email(self::USER_EMAIL);

        $this->userRepositoryInterface
            ->expects($this->once())
            ->method('removeUser')
            ->with(self::USER_EMAIL);

        $this->removeUserService->execute($email);
    }
}
