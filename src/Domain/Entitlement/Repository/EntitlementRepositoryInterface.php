<?php

namespace App\Domain\Entitlement\Repository;

interface EntitlementRepositoryInterface
{
    public function remove(string $resourceId, string $email);

    public function save(string $resourceId, string $email);
}
