<?php

namespace App\Domain\Entitlement\Dto;

use App\Domain\Resource\Dto\ResourceId;
use App\Domain\User\Dto\Email;

class Entitlement
{
    /**
     * @var ResourceId
     */
    private $resourceId;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var \DateTime|null
     */
    private $expireDate;

    public function __construct(ResourceId $resourceId, Email $email, \DateTime $expireDate = null)
    {
        $this->resourceId = $resourceId;
        $this->email = $email;
        $this->expireDate = $expireDate;
    }

    /**
     * @return ResourceId
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }
}
