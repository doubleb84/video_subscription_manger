<?php

namespace App\Domain\Entitlement\Infrastructure;

use App\Domain\Entitlement\Repository\EntitlementRepositoryInterface;
use Snc\RedisBundle\Client\Phpredis\Client;

class RedisEntitlementRepository implements EntitlementRepositoryInterface
{
    const USER_RESOURCE_PREFIX = ':Resource';
    const EXPIRE_DATE_PREFIX = ':ExpireDate';
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var Client
     */
    private $redisClient;

    /**
     * RedisEntitlementRepository constructor.
     */
    public function __construct(Client $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function remove(string $resourceId, string $email)
    {
        $this->redisClient->sRemove($email.self::USER_RESOURCE_PREFIX, $resourceId);
        if ($this->redisClient->hExists($email.self::EXPIRE_DATE_PREFIX, $resourceId)) {
            $this->redisClient->hDel($email.self::EXPIRE_DATE_PREFIX, $resourceId);
        }
    }

    public function save(string $resourceId, string $email)
    {
        $this->redisClient->sAdd($email.self::USER_RESOURCE_PREFIX, $resourceId);
    }

    /**
     * @return array|mixed
     */
    public function getExpireDate(string $email, string $resourceId)
    {
        return $this->redisClient->hMGet($email.self::EXPIRE_DATE_PREFIX, [$resourceId]);
    }

    public function addExpireDate(string $email, string $resourceId, \DateTime $expireDate)
    {
        $expireDateString = $expireDate->format(self::DATE_TIME_FORMAT);
        $this->redisClient->hMSet($email.self::EXPIRE_DATE_PREFIX, [$resourceId => $expireDateString]);
    }
}
