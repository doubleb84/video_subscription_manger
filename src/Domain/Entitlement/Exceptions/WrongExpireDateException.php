<?php

namespace App\Domain\Entitlement\Exceptions;

class WrongExpireDateException extends EntitlementDomainException
{
    protected $message = Messages::EXPIRE_DATE_EXCEPTION;
}
