<?php

namespace App\Domain\Entitlement\Exceptions;

class ResourceNotExistException extends EntitlementDomainException
{
    protected $message = Messages::RESOURCE_NOT_EXIST;
}
