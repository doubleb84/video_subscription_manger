<?php

namespace App\Domain\Entitlement\Exceptions;

class UserNotExistException extends EntitlementDomainException
{
    protected $message = Messages::USER_NOT_EXIST;
}
