<?php

namespace App\Domain\Entitlement\Exceptions;

class Messages
{
    const RESOURCE_NOT_EXIST = 'Resource not exist';
    const USER_NOT_EXIST = 'User not exist';
    const EXPIRE_DATE_EXCEPTION = 'Expire date is wrong';
}
