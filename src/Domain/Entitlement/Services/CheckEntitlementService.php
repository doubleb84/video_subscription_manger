<?php

namespace App\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use DateTime;

class CheckEntitlementService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var ResourceRepositoryInterface
     */
    private $resourceRepository;

    private $resources = [];

    public function __construct(UserRepositoryInterface $userRepository, ResourceRepositoryInterface $resourceRepository)
    {
        $this->userRepository = $userRepository;
        $this->resourceRepository = $resourceRepository;
    }

    /**
     * @return bool
     *
     * @throws ResourceNotExistException
     */
    public function execute(Entitlement $entitlement)
    {
        if (!$this->resourceRepository->isResourceExist($entitlement->getResourceId()->id())) {
            throw new ResourceNotExistException();
        }

        $this->resources = $this->userRepository->getUserResources($entitlement->getEmail()->getEmail());

        return $this->checkUserResources($entitlement->getEmail()->getEmail(), $entitlement->getResourceId()->id());
    }

    /**
     * @return bool
     *
     * @throws \Exception
     */
    private function checkUserResources(string $username, string $resourceId)
    {
        $expireDate = $this->userRepository->getExpireDateOfUserResource($username, $resourceId);
        $timeComparisionResult = true;
        if (false !== $expireDate[$resourceId]) {
            $timeComparisionResult = (new DateTime($expireDate[$resourceId])) > (new DateTime());
        }

        if (in_array($resourceId, $this->resources) && $timeComparisionResult) {
            return true;
        } else {
            return $this->checkParent($username, $resourceId);
        }
    }

    /**
     * @return bool
     *
     * @throws \Exception
     */
    private function checkParent(string $username, string $resourceId)
    {
        $parentId = $this->resourceRepository->getResourceParentId($resourceId);
        if (null == $parentId) {
            return false;
        } else {
            return $this->checkUserResources($username, $parentId);
        }
    }
}
