<?php

namespace App\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Entitlement\Exceptions\UserNotExistException;
use App\Domain\Entitlement\Repository\EntitlementRepositoryInterface;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class RemoveEntitlementService
{
    private $userRepository;

    private $resourceRepository;

    private $entitlementRepository;

    public function __construct(
        EntitlementRepositoryInterface $entitlementRepository,
        UserRepositoryInterface $userRepository,
        ResourceRepositoryInterface $resourceRepository
    ) {
        $this->entitlementRepository = $entitlementRepository;
        $this->resourceRepository = $resourceRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws ResourceNotExistException
     * @throws UserNotExistException
     */
    public function execute(Entitlement $entitlement)
    {
        $resourceExist = $this->resourceRepository->isResourceExist($entitlement->getResourceId()->id());
        if (!$resourceExist) {
            throw new ResourceNotExistException();
        }

        $userExist = $this->userRepository->isUserExist($entitlement->getEmail()->getEmail());
        if (!$userExist) {
            throw  new UserNotExistException();
        }

        $this->entitlementRepository->remove(
            $entitlement->getResourceId()->id(),
            $entitlement->getEmail()->getEmail()
        );
    }
}
