<?php

namespace App\Domain\Entitlement\Services;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\ResourceNotExistException;
use App\Domain\Entitlement\Exceptions\UserNotExistException;
use App\Domain\Entitlement\Exceptions\WrongExpireDateException;
use App\Domain\Resource\Infrastructure\RedisResourceRepository;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use App\Domain\User\Infrastructure\RedisUserRepository;
use App\Domain\User\Repository\UserRepositoryInterface;

class CreateEntitlementService
{
    /**
     * @var RedisUserRepository
     */
    private $userRepository;

    /**
     * @var RedisResourceRepository
     */
    private $resourceRepository;

    /**
     * CreateEntitlementService constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository, ResourceRepositoryInterface $resourceRepository)
    {
        $this->userRepository = $userRepository;
        $this->resourceRepository = $resourceRepository;
    }

    /**
     * @throws ResourceNotExistException
     * @throws UserNotExistException
     * @throws WrongExpireDateException
     */
    public function execute(Entitlement $entitlement)
    {
        $isResourceExist = $this->resourceRepository->isResourceExist($entitlement->getResourceId()->id());
        if (!$isResourceExist) {
            throw new ResourceNotExistException();
        }

        $isUserExist = $this->userRepository->isUserExist($entitlement->getEmail()->getEmail());
        if (!$isUserExist) {
            throw new UserNotExistException();
        }

        if (!is_null($entitlement->getExpireDate())) {
            if ($entitlement->getExpireDate() < new \DateTime()) {
                throw new WrongExpireDateException();
            }

            $this->userRepository->addExpireTimeToUserResource(
                $entitlement->getEmail()->getEmail(),
                $entitlement->getResourceId()->id(),
                $entitlement->getExpireDate()
            );
        }

        $this->userRepository->addResourceToUser(
            $entitlement->getEmail()->getEmail(),
            $entitlement->getResourceId()->id()
        );
    }
}
