<?php

namespace App\Domain\Resource\Dto;

class ResourceId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ResourceId constructor.
     *
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        $this->id = null === $id ? \Ramsey\Uuid\Uuid::uuid4()->toString() : $id;
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    public function equals(ResourceId $resourceId): bool
    {
        return $this->id() === $resourceId->id();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}
