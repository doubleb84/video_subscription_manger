<?php

namespace App\Domain\Resource\Dto;

class ResourceName
{
    private $resourceName;

    public function __construct(string $resourceName)
    {
        $this->resourceName = $resourceName;
    }

    public function name()
    {
        return $this->resourceName;
    }

    public function equeals(ResourceName $resourceName)
    {
        return $this->name() === $resourceName->name();
    }

    public function __toString()
    {
        return $this->resourceName;
    }
}
