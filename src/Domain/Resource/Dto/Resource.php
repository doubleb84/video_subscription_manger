<?php

namespace App\Domain\Resource\Dto;

class Resource
{
    private $resourceId;

    private $resourceName;

    public function __construct(ResourceId $resourceId, ResourceName $resourceName)
    {
        $this->resourceId = $resourceId;
        $this->resourceName = $resourceName;
    }

    public function getResourceId()
    {
        return $this->resourceId;
    }

    public function getResourceName()
    {
        return $this->resourceName;
    }
}
