<?php

namespace App\Domain\Resource\Repository;

use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Dto\ResourceName;

interface ResourceRepositoryInterface
{
    public function save(ResourceId $resourceId, ResourceName $resourceName);

    public function createRelationshipBetweenResources(string $childResourceId, string $parentResourceId);

    public function getResourceParentId(string $resourceId);

    public function isResourceExist(string $resourceId);
}
