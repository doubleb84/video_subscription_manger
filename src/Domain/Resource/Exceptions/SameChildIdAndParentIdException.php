<?php

namespace App\Domain\Resource\Exceptions;

class SameChildIdAndParentIdException extends ResourceDomainException
{
    protected $message = Messages::SAME_CHILD_AND_PARENT_ID;
}
