<?php

namespace App\Domain\Resource\Exceptions;

class Messages
{
    const INVALID_UUID_MESSAGE = 'Invalid uuid. PLease pick other one';
    const EMPTY_RESOURCE_NAME_MESSAGE = 'Resource name is empty. Pick some name';
    const FAIL_RESOURCE_CREATION_MESSAGE = 'Resource not created';
    const RESOURCE_WITH_THIS_ID_ALREADY_EXIST = 'Resource with this id already exist.';
    const SAME_CHILD_AND_PARENT_ID = 'Child and parent should not have same id';
    const RESOURCE_NOT_EXIST = 'Resource not exist';
}
