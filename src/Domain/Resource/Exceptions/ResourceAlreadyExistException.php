<?php

namespace App\Domain\Resource\Exceptions;

class ResourceAlreadyExistException extends ResourceDomainException
{
    protected $message = Messages::RESOURCE_WITH_THIS_ID_ALREADY_EXIST;
}
