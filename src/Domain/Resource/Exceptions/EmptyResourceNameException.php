<?php

namespace App\Domain\Resource\Exceptions;

class EmptyResourceNameException extends ResourceDomainException
{
    protected $message = Messages::EMPTY_RESOURCE_NAME_MESSAGE;
}
