<?php

namespace App\Domain\Resource\Exceptions;

class CreateResourceException extends ResourceDomainException
{
    protected $message = Messages::FAIL_RESOURCE_CREATION_MESSAGE;
}
