<?php

namespace App\Domain\Resource\Exceptions;

class ResourceNotExistException extends ResourceDomainException
{
    private $messages = Messages::RESOURCE_NOT_EXIST;
}
