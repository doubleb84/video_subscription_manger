<?php

namespace App\Domain\Resource\Exceptions;

class InvalidUuidException extends ResourceDomainException
{
    protected $message = Messages::INVALID_UUID_MESSAGE;
}
