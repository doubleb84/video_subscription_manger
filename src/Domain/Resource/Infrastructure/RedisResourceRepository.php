<?php

namespace App\Domain\Resource\Infrastructure;

use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Dto\ResourceName;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use Snc\RedisBundle\Client\Phpredis\Client;

class RedisResourceRepository implements ResourceRepositoryInterface
{
    private $redisClient;

    public function __construct(Client $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function save(ResourceId $resourceId, ResourceName $resourceName)
    {
        return $this->redisClient->hmset($resourceId->id(), ['name' => $resourceName->name(), 'parent_id' => null]);
    }

    public function isResourceExist(string $resourceId)
    {
        return $this->redisClient->exists($resourceId);
    }

    public function createRelationshipBetweenResources(string $childResourceId, string $parentResourceId)
    {
        $this->redisClient->hMSet($childResourceId, ['parent_id' => $parentResourceId]);
    }

    public function getResourceParentId(string $resourceId)
    {
        return $this->redisClient->hMGet($resourceId, ['parent_id'])['parent_id'];
    }
}
