<?php

namespace App\Domain\Resource\Services;

use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Exceptions\ResourceNotExistException;
use App\Domain\Resource\Exceptions\SameChildIdAndParentIdException;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;

/**
 * Class CreateResourceRelationshipService.
 */
class CreateResourceRelationshipService
{
    private $redisResourceRepository;

    /**
     * CreateResourceRelationshipService constructor.
     */
    public function __construct(ResourceRepositoryInterface $resourceRepository)
    {
        $this->redisResourceRepository = $resourceRepository;
    }

    /**
     * @throws ResourceNotExistException
     * @throws SameChildIdAndParentIdException
     */
    public function execute(ResourceId $childResourceId, ResourceId $parentResourceId)
    {
        if ($childResourceId->equals($parentResourceId)) {
            throw new SameChildIdAndParentIdException();
        }

        $childExist = $this->redisResourceRepository->isResourceExist($childResourceId->id());
        $parentExist = $this->redisResourceRepository->isResourceExist($parentResourceId->id());

        if (!$childExist || !$parentExist) {
            throw new ResourceNotExistException();
        }

        $this->redisResourceRepository->getResourceParentId($childResourceId->id());

        $this->redisResourceRepository->createRelationshipBetweenResources($childResourceId->id(), $parentResourceId->id());
    }
}
