<?php

namespace App\Domain\Resource\Services;

use App\Domain\Resource\Dto\Resource;
use App\Domain\Resource\Exceptions\CreateResourceException;
use App\Domain\Resource\Exceptions\EmptyResourceNameException;
use App\Domain\Resource\Exceptions\InvalidUuidException;
use App\Domain\Resource\Exceptions\ResourceAlreadyExistException;
use App\Domain\Resource\Repository\ResourceRepositoryInterface;
use Ramsey\Uuid\Rfc4122\Validator;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;

class CreateResourceService
{
    private $resourceRespository;

    /**
     * CreateResourceService constructor.
     */
    public function __construct(ResourceRepositoryInterface $resourceRepository)
    {
        $this->resourceRespository = $resourceRepository;
    }

    public function execute(Resource $resource)
    {
        $factory = new UuidFactory();
        $factory->setValidator(new Validator());

        Uuid::setFactory($factory);
        $uuidValidationResult = Uuid::isValid($resource->getResourceId()->id());
        $resourceNameValidationResult = empty($resource->getResourceName()->name());

        $resourceExist = $this->resourceRespository->isResourceExist($resource->getResourceId()->id());

        if ($resourceExist) {
            throw new ResourceAlreadyExistException();
        }

        if (!$uuidValidationResult) {
            throw new InvalidUuidException();
        }

        if ($resourceNameValidationResult) {
            throw new EmptyResourceNameException();
        }

        $result = $this->resourceRespository->save($resource->getResourceId(), $resource->getResourceName());
        if (true === $result) {
            return $resource;
        }

        throw new CreateResourceException();
    }
}
