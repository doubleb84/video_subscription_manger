<?php

namespace App\Domain\User\Services;

use App\Domain\User\Dto\Email;
use App\Domain\User\Repository\UserRepositoryInterface;

class RemoveUserService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $email
     */
    public function execute(Email $email)
    {
        return $this->userRepository->removeUser($email->getEmail());
    }
}
