<?php

namespace App\Domain\User\Services;

use App\Domain\User\Dto\Email;
use App\Domain\User\Exceptions\Messages;
use App\Domain\User\Exceptions\UserAlreadyExistException;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class CreateUserService.
 */
class CreateUserService
{
    private $redisUserRepository;

    public function __construct(UserRepositoryInterface $redisUserRepository)
    {
        $this->redisUserRepository = $redisUserRepository;
    }

    public function execute(Email $emailDto)
    {
        $isExist = $this->redisUserRepository->isUserExist($emailDto->getEmail());
        if ($isExist) {
            throw new UserAlreadyExistException(Messages::USER_ALREADY_EXIST);
        }
        $this->redisUserRepository->createUser($emailDto->getEmail());
    }
}
