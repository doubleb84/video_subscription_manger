<?php

namespace App\Domain\User\Infrastructure;

use App\Domain\User\Repository\UserRepositoryInterface;
use Snc\RedisBundle\Client\Phpredis\Client;

class RedisUserRepository implements UserRepositoryInterface
{
    const USER_RESOURCE_PREFIX = ':Resource';
    const EXPIRE_DATE_PREFIX = ':ExpireDate';

    private $redisClient;

    /**
     * RedisUserRepository constructor.
     */
    public function __construct(Client $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function createUser(string $email)
    {
        $this->redisClient->sAdd('users', $email);
    }

    /**
     * @return bool|mixed
     */
    public function isUserExist(string $email)
    {
        return $this->redisClient->sIsMember('users', $email);
    }

    /**
     * @return array|mixed
     */
    public function getUserResources(string $email)
    {
        return $this->redisClient->sMembers($email.self::USER_RESOURCE_PREFIX);
    }

    public function removeUser(string $email)
    {
        $this->redisClient->sRemove('users', $email);
        $this->redisClient->del($email.self::EXPIRE_DATE_PREFIX);
        $this->redisClient->del($email.self::USER_RESOURCE_PREFIX);
    }

    /**
     * @return array|mixed
     */
    public function getExpireDateOfUserResource(string $email, string $resourceId)
    {
        return $this->redisClient->hMGet($email.self::EXPIRE_DATE_PREFIX, [$resourceId]);
    }

    public function addResourceToUser(string $email, string $resourceId)
    {
        $this->redisClient->sAdd($email.self::USER_RESOURCE_PREFIX, $resourceId);
    }

    public function addExpireTimeToUserResource(string $email, string $resourceId, \DateTime $expireDate)
    {
        $expireDateString = $expireDate->format('Y-m-d H:i:s');
        $this->redisClient->hMSet($email.self::EXPIRE_DATE_PREFIX, [$resourceId => $expireDateString]);
    }
}
