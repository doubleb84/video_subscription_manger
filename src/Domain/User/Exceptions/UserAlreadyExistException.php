<?php

namespace App\Domain\User\Exceptions;

class UserAlreadyExistException extends UserDomainException
{
    protected $message = Messages::USER_ALREADY_EXIST;
}
