<?php

namespace App\Controller;

use App\Domain\User\Dto\Email;
use App\Domain\User\Exceptions\UserDomainException;
use App\Domain\User\Services\CreateUserService;
use App\Domain\User\Services\RemoveUserService;
use App\Helper\FormErrorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email as EmailConstrain;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserController extends AbstractController
{
    use FormErrorHelper;

    /** @var CreateUserService */
    private $createUserService;

    /** @var RemoveUserService */
    private $removeUserService;

    public function __construct(CreateUserService $createUserService, RemoveUserService $removeUserService)
    {
        $this->createUserService = $createUserService;
        $this->removeUserService = $removeUserService;
    }

    /**
     * @Route("/user", name="post_user", methods={"POST"})
     */
    public function createUser(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $form = $this->createUserForm();
        $form->submit($data);
        if ($form->isValid()) {
            $username = $form->get('username')->getData();
            $emailDto = new Email($username);
            try {
                $this->createUserService->execute($emailDto);

                return new JsonResponse(['status' => 'ok', 'data' => ['message' => 'User created']], Response::HTTP_OK);
            } catch (UserDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'data' => ['message' => $exception->getMessage()]], Response::HTTP_BAD_REQUEST);
            }
        }
        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/user", name="delete_user", methods={"DELETE"})
     */
    public function removeUser(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createUserForm();
        $form->submit($data);
        if ($form->isValid()) {
            $username = $form->get('username')->getData();
            $emailDto = new Email($username);
            try {
                $this->removeUserService->execute($emailDto);

                return new JsonResponse(['status' => 'ok', 'data' => ['message' => 'User removed']], Response::HTTP_OK);
            } catch (UserDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'data' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
            }
        }
        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors], Response::HTTP_BAD_REQUEST);
    }

    private function createUserForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('username', TextType::class, ['constraints' => [new NotBlank(), new EmailConstrain()]])
            ->getForm();
    }
}
