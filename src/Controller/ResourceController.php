<?php

namespace App\Controller;

use App\Domain\Resource\Dto\Resource;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\Resource\Dto\ResourceName;
use App\Domain\Resource\Exceptions\ResourceDomainException;
use App\Domain\Resource\Services\CreateResourceRelationshipService;
use App\Domain\Resource\Services\CreateResourceService;
use App\Helper\FormErrorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Uuid;

/**
 * Class ResourceController.
 */
class ResourceController extends AbstractController
{
    use FormErrorHelper;

    /** @var CreateResourceService */
    private $createResourceService;

    /** @var CreateResourceRelationshipService */
    private $createResourceRelationshipService;

    public function __construct(
        CreateResourceService $createResourceService,
        CreateResourceRelationshipService $createResourceRelationshipService
    ) {
        $this->createResourceService = $createResourceService;
        $this->createResourceRelationshipService = $createResourceRelationshipService;
    }

    /**
     * @Route("/resource", name="post_resource", methods={"POST"})
     */
    public function createResource(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $form = $this->createResourceForm();
        $form->submit($data);

        if ($form->isValid()) {
            try {
                $resourceName = new ResourceName($form->get('resource_name')->getData());
                $resourceId = new ResourceId($form->get('resource_id')->getData());
                $this->createResourceService->execute(new Resource($resourceId, $resourceName));
            } catch (ResourceDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'data' => ['error' => $exception->getMessage()]], Response::HTTP_BAD_REQUEST);
            }
        }

        return new JsonResponse(['status' => 'success', 'date' => ['resource_id' => $resourceId->id()]], Response::HTTP_OK);
    }

    /**
     * @Route("/resource/relationship", name="post_resource_relationship", methods={"POST"})
     */
    public function defineRelationship(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createResourceRelationshipForm();
        $form->submit($data);
        $isValid = $form->isValid();
        if ($isValid) {
            try {
                $childResourceId = new ResourceId($form->get('child_id')->getData());
                $parentResourceId = new ResourceId($form->get('parent_id')->getData());
                $this->createResourceRelationshipService->execute($childResourceId, $parentResourceId);
            } catch (ResourceDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(['status' => 'success', 'data' => ['message' => 'Relationship created']], Response::HTTP_OK);
        }

        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return FormInterface
     */
    private function createResourceForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('resource_name', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('resource_id', TextType::class, ['constraints' => [new Uuid()], 'required' => false, 'empty_data' => null])
            ->getForm();
    }

    /**
     * @return FormInterface
     */
    private function createResourceRelationshipForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('child_id', TextType::class, ['constraints' => [new NotBlank(), new Uuid()]])
            ->add('parent_id', TextType::class, ['constraints' => [new NotBlank(), new Uuid()]])
            ->getForm();
    }
}
