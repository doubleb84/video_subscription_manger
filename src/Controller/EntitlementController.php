<?php

namespace App\Controller;

use App\Domain\Entitlement\Dto\Entitlement;
use App\Domain\Entitlement\Exceptions\EntitlementDomainException;
use App\Domain\Entitlement\Services\CheckEntitlementService;
use App\Domain\Entitlement\Services\CreateEntitlementService;
use App\Domain\Entitlement\Services\RemoveEntitlementService;
use App\Domain\Resource\Dto\ResourceId;
use App\Domain\User\Dto\Email;
use App\Helper\FormErrorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Uuid;

class EntitlementController extends AbstractController
{
    use FormErrorHelper;

    private $createEntitlementService;

    private $checkEntitlementService;

    private $removeEntitlementService;

    public function __construct(
        CreateEntitlementService $createEntitlementService,
        CheckEntitlementService $checkEntitlementService,
        RemoveEntitlementService $removeEntitlementService
    ) {
        $this->createEntitlementService = $createEntitlementService;
        $this->checkEntitlementService = $checkEntitlementService;
        $this->removeEntitlementService = $removeEntitlementService;
    }

    /**
     * @Route("/entitlement", name="entitlement_post", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createEntitlementForm();
        $form->submit($data);
        $isValid = $form->isValid();

        if ($isValid) {
            try {
                $username = new Email($form->get('username')->getData());
                $resource = new ResourceId($form->get('resource_id')->getData());
                $expireDate = $form->get('expire_date')->getData();
                $entitlement = new Entitlement($resource, $username, $expireDate);
                $this->createEntitlementService->execute($entitlement);

                return new JsonResponse(['status' => 'ok', 'data' => ['message' => 'Entitlement create']], Response::HTTP_OK);
            } catch (EntitlementDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'data' => ['message' => $exception->getMessage()]], Response::HTTP_BAD_REQUEST);
            }
        }
        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/entitlement", name="entitlement_delete", methods={"DELETE"})
     */
    public function remove(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createRemoveEntitlementForm();
        $form->submit($data);
        $isValid = $form->isValid();

        if ($isValid) {
            try {
                $username = new Email($form->get('username')->getData());
                $resource = new ResourceId($form->get('resource_id')->getData());
                $entitlement = new Entitlement($resource, $username);
                $this->removeEntitlementService->execute($entitlement);

                return new JsonResponse(['status' => 'ok', 'data' => ['message' => 'Entitlement removed']], Response::HTTP_OK);
            } catch (EntitlementDomainException $exception) {
                return new JsonResponse(['status' => 'error', 'data' => ['message' => $exception->getMessage()]], Response::HTTP_BAD_REQUEST);
            }
        }
        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/entitlement/check", name="entitlement_check", methods={"GET"})
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function checkEntitlement(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createEntitlementForm();
        $form->submit($data);
        $isValid = $form->isValid();

        if ($isValid) {
            $username = new Email($form->get('username')->getData());
            $resourceId = new ResourceId($form->get('resource_id')->getData());
            $entitlement = new Entitlement($resourceId, $username);
            $existEntitlement = $this->checkEntitlementService->execute($entitlement);

            return new JsonResponse(['status' => 'ok', 'data' => ['is_allow' => $existEntitlement]], Response::HTTP_OK);
        }

        $errors = $this->extractFormErrors($form);

        return new JsonResponse(['status' => 'error', 'data' => $errors]);
    }

    private function createRemoveEntitlementForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('username', TextType::class, ['constraints' => [new NotBlank(), new EmailConstraint()]])
            ->add('resource_id', TextType::class, ['constraints' => [new NotBlank(), new Uuid()]])
            ->getForm();
    }

    private function createEntitlementForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('username', TextType::class, ['constraints' => [new NotBlank(), new EmailConstraint()]])
            ->add('resource_id', TextType::class, ['constraints' => [new NotBlank(), new Uuid()]])
            ->add('expire_date', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'required' => true,
                'format' => 'yyyy-MM-dd HH:mm:ss', ])
            ->getForm();
    }
}
