<?php

namespace App\Helper;

use Symfony\Component\Form\FormInterface;

trait FormErrorHelper
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public function extractFormErrors(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->extractFormErrors($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}
